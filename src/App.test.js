import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
test('sum numbers', () => {
	expect(1+1).toEqual(2);
	expect(3+3).toEqual(6);
});
test('sub numbers', () => {
	expect(8-4).toEqual(4);
	expect(9-6).toEqual(3);
});
